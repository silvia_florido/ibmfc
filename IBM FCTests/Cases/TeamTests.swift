//
//  TeamTests.swift
//  IBM FCTests
//
//  Created by Silvia Florido on 09/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import XCTest

@testable import IBM_FC

class TeamTests: XCTestCase {
    
    var team: Team!
    
    override func setUp() {
        super.setUp()
        team = Team(id: "1", name: "Romania", pathToFlag: "162-Romania.png")
    }
   
    override func tearDown() {
        super.tearDown()
        team = nil
    }
    
    
    func testGameResultsSuccess() {
        team.setGameResults(result: .win, goals: 2)
        XCTAssertEqual(team.totalPoints, 3)
        
        team.setGameResults(result: .even, goals: 0)
        XCTAssertEqual(team.totalPoints, 4)
        
        team.setGameResults(result: .defeat, goals: 1)
        XCTAssertEqual(team.totalPoints, 4)
        
        XCTAssertEqual(team.goals, 3)
        
        team.setGameResults(result: .even, goals: 0)
        XCTAssertNotEqual(team.goals, 4)
        XCTAssertEqual(team.evens, 2)
    }
    
    func testLoadData() {
        let image = UIImage(named: team.pathToFlag)
        XCTAssertNotNil(image)
    }
    
}
