//
//  GameTests.swift
//  IBM FCTests
//
//  Created by Silvia Florido on 09/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import XCTest

@testable import IBM_FC
class GameTests: XCTestCase {
    
    var game: Game!
    var homeTeam: Team!
    var awayTeam: Team!
    
    
    override func setUp() {
        super.setUp()
        homeTeam = Team(id: "2", name: "Albania", pathToFlag: "2-Albania.png")
        awayTeam = Team(id: "11", name: "Aruba", pathToFlag: "11-Aruba")
        game = Game(homeTeam: homeTeam, awayTeam: awayTeam, homeTeamScore: 1, awayTeamScore: 2)
        game.calculateGameResults()
    }
    
    override func tearDown() {
        game = nil
        homeTeam = nil
        awayTeam = nil
        super.tearDown()
    }
    
    func testGameResultsSucess() {
        XCTAssertEqual(homeTeam.defeats, 1)
        XCTAssertEqual(homeTeam.wins, 0)

        XCTAssertEqual(awayTeam.defeats, 0)
        XCTAssertEqual(awayTeam.wins, 1)
        
        XCTAssertEqual(awayTeam.evens, 0)
        XCTAssertEqual(homeTeam.evens, 0)
        
    }
    
    

    
    
    
    
    
}








