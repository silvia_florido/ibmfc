//
//  DesignableTextField.swift
//  IBM FC
//
//  Created by Silvia Florido on 06/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableTextField: UITextField {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftImgWidth: CGFloat = 0 {
        didSet {
            updateView()
        }
    }

    @IBInspectable var leftImgHeight: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftImgLeftPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }

    @IBInspectable var leftImgRightPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = .always
            
            let imageView = UIImageView(frame: CGRect(x: leftImgLeftPadding, y: 0, width: leftImgWidth, height: leftImgHeight))
            imageView.image = image
            
            let totalWidth = leftImgLeftPadding + imageView.frame.width + leftImgRightPadding
            let view = UIView(frame: CGRect(x: 0, y: 0, width: totalWidth, height: imageView.frame.height))
            view.addSubview(imageView)
            leftView = view
        } else {
            leftViewMode = .never
        }
    }
   
}

























