//
//  TeamTableViewCell.swift
//  IBM FC
//
//  Created by Silvia Florido on 12/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {

    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var goalsLabel: UILabel!
    @IBOutlet weak var winsLabel: UILabel!
    @IBOutlet weak var evensLabel: UILabel!
    @IBOutlet weak var defeatsLabel: UILabel!
    @IBOutlet weak var totalPointsLabel: UILabel!

}
