//  CountriesListViewController.swift
//  IBM FC
//
//  Created by Silvia Florido on 09/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit


struct CountryData {
    let id: String
    let name: String
    let pathToFlagImg: String
}


class CountriesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var countriesTableView: UITableView!
    
    let cellID = "countriesListCell"
    
    var countriesData: [CountryData] = []
    var filteredCountries: [CountryData] = []
    var model: [CountryData] {
        return isSearching ? filteredCountries : countriesData
    }
    var isSearching: Bool = false
    var selectedCountry: CountryData?
    var selectedCountryCallBack: ((_ selected: CountryData) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        loadCountriesData()
    }
    
    // MARK: - Actions
    @IBAction func okButton(_ sender: UIBarButtonItem) {
        if selectedCountry != nil {
            selectedCountryCallBack?(selectedCountry!)
            dismissController()
        }
    }

    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        dismissController()
    }

    // MARK: - Helpers
    private func loadCountriesData() {
        let fileManager = FileManager.default
        let bundleURL = Bundle.main.resourceURL
        let assetURL = bundleURL?.appendingPathComponent("Flags")
        if let contents = try? fileManager.contentsOfDirectory(at: assetURL!, includingPropertiesForKeys: [URLResourceKey.nameKey, URLResourceKey.isDirectoryKey], options: .skipsHiddenFiles) {
            countriesData = contents.flatMap{ (item) in
                let path = item.lastPathComponent
                let pathComponents = item.lastPathComponent.components(separatedBy: "-")
                if let id = pathComponents.first, let nameWithExtension = pathComponents.last {
                    let name = nameWithExtension.replacingOccurrences(of: ".png", with: "").replacingOccurrences(of: "_", with: " ")
                    return CountryData(id: id, name: name, pathToFlagImg: path)
                } else {
                    return nil
                }
                }.sorted{ $0.name < $1.name }
        }
    }

    private func resize(image: UIImage) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 41.0, height: 28.0), true, 0)
        image.draw(in: CGRect(x: 0, y: 0, width: 41.0, height: 28.0))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func dismissController() {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        if model.count >= indexPath.row {
            let country = model[indexPath.row]
            cell.textLabel?.text = country.name
            if let image = UIImage(named: country.pathToFlagImg) {
                cell.imageView?.image = resize(image: image)
                cell.imageView?.contentMode = .scaleAspectFit
            }
            if selectedCountry?.name == country.name {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            selectedCountry = model[indexPath.row]
            searchBar.text = selectedCountry?.name
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            isSearching = false
            view.endEditing(true)
        } else {
            isSearching = true
            searchBar.becomeFirstResponder()
            filteredCountries = countriesData.filter { $0.name.contains(searchText)}
        }
        countriesTableView.reloadData()
    }
    
    
}
