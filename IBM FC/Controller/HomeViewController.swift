//
//  HomeViewController.swift
//  IBM FC
//
//  Created by Silvia Florido on 09/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var teamsTableView: UITableView!
    
    let teamCellID = "teamTableCell"
    var playingTeams = Set<Team>() {
        didSet {
            orderedPlayingTeams = playingTeams.sorted(by: { (team1, team2) -> Bool in
                return team1.name < team2.name
            }).sorted(by: { (team1, team2) -> Bool in
                return team1.totalPoints > team2.totalPoints
            })
        }
    }
    
    var orderedPlayingTeams: [Team]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let client = Client()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        teamsTableView.reloadData()
    }
   
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addGameSegue" {
            if let navigation = (segue.destination as? UINavigationController),
                let destination = navigation.topViewController as? AddGameViewController {
                destination.playingTeams = self.playingTeams
                destination.playingTeamsCallBack = { [weak self] playingTeams in
                    self?.playingTeams = playingTeams
                }
            }
        }
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playingTeams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: teamCellID, for: indexPath) as! TeamTableViewCell
        
        if let team = orderedPlayingTeams?[indexPath.row] {
            
            cell.nameLabel.text = team.name
            cell.totalPointsLabel.text = String(team.totalPoints)
            cell.winsLabel.text = String(team.wins)
            cell.evensLabel.text = String(team.evens)
            cell.defeatsLabel.text = String(team.defeats)
            cell.goalsLabel.text = String(team.goals)
            cell.flagImageView.image = UIImage(named: team.pathToFlag)
            // test image
        }
        
        
        
        return cell
    }
    
}
















