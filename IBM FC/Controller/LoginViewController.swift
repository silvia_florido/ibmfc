//
//  LoginViewController.swift
//  IBM FC
//
//  Created by Silvia Florido on 12/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userTextField: DesignableTextField!
    @IBOutlet weak var passwordTextField: DesignableTextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    lazy var client: Client = {
        return Client()
    }()
    
    @IBAction func loginButton(_ sender: UIButton) {
        guard let username = userTextField.text,
            !username.isEmpty,
            isValidEmail(email: username) else {
                // show error message
                print("Digite um email valido.")
                return
        }
        guard let password = passwordTextField.text,
            password.count > 7 else {
                // show error message
                print("Senha deve ter no mínimo 8 caracteres caracteres.")
                return
        }
        view.endEditing(true)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        client.authenticate(with: username, pass: password) { (success, error) in
            if success  {
                DispatchQueue.main.async {
                    self.completeLogin()
                }
            } else {
                // back and error
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                print(error ?? "something went wrong")
            }
        }
    }
    
    private func completeLogin() {
        let navController = storyboard?.instantiateViewController(withIdentifier: "HomeNavigationController") as! UINavigationController
        present(navController, animated: true, completion: nil)
    }
    
    private func isValidEmail(email:String) -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    
     // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    

    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
