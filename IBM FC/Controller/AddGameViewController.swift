//
//  AddCountryViewController.swift
//  IBM FC
//
//  Created by Silvia Florido on 09/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit



class AddGameViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var awayButton: UIButton!
    
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var awayLabel: UILabel!
    
    @IBOutlet weak var homeScoreTextField: UITextField!
    @IBOutlet weak var awayScoreTextField: UITextField!
    
    var pressedButton: UIButton?
    var homeCountry: CountryData?
    var awayCountry: CountryData?
    
    var playingTeams: Set<Team>!
    
    var playingTeamsCallBack: ((_ playingTeams: Set<Team>) -> Void)!
    
    var games: [Game] = []
    
    
    @IBAction func addTeamButton(_ sender: UIButton) {
        pressedButton = sender
        performSegue(withIdentifier: "showCountriesList", sender: nil)
    }
    
    @IBAction func okButton(_ sender: UIBarButtonItem) {
        setupGame()
        playingTeamsCallBack(playingTeams)
        dismissController()
    }
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        dismissController()
    }
    
    func dismissController() {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
        
    }
    // MARK: - Helpers
    private func setupGame() {
        guard let home = homeCountry, let away = awayCountry else {
            print("Falta adicionar países.")
            return
        }
        guard  home.id != away.id else {
            print("Escolha dois países diferentes.")
            return
        }
        guard let homeScore = homeScoreTextField.text,
            let awayScore = awayScoreTextField.text,
            let homeScoreInt = Int(homeScore),
            let awayScoreInt = Int(awayScore)
            else {
                return
        }
        let homeTeam = setupTeam(for: home)
        let awayTeam = setupTeam(for: away)
        let game = Game(homeTeam: homeTeam, awayTeam: awayTeam, homeTeamScore: homeScoreInt, awayTeamScore: awayScoreInt)
        game.calculateGameResults()
        games.append(game)
        _ = homeTeam.totalPoints
    }
    
    
    private func setupTeam(for country: CountryData) -> Team {
        var team = playingTeams.filter{$0.id == country.id}.first
        if team == nil {
            team = Team(id: country.id, name: country.name, pathToFlag: country.pathToFlagImg)
            playingTeams.insert(team!)
        }
        return team!
    }
    
    private func updateUI(for selectedCountry: CountryData) {
        if pressedButton == homeButton {
            homeCountry = selectedCountry
            homeLabel.text = homeCountry?.name
            homeLabel.textColor = UIColor.darkGray
        } else if pressedButton == awayButton {
            awayCountry = selectedCountry
            awayLabel.text = awayCountry?.name
            awayLabel.textColor = UIColor.darkGray
        }
        if let image = UIImage(named: selectedCountry.pathToFlagImg) {
            pressedButton?.setImage(image, for: .normal)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCountriesList" {
            if let navigation = (segue.destination as? UINavigationController),
                let destination = navigation.topViewController as? CountriesListViewController {
                destination.selectedCountryCallBack = { [weak self] selectedCountry in
                    self?.updateUI(for: selectedCountry)
                }
            }
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    
}


