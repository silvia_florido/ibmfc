//
//  Client.swift
//  IBM FC
//
//  Created by Silvia Florido on 12/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation

public typealias JSONDictionary = [String: Any]
public typealias ParemetersDictionary = [String: Any]
public typealias HTTPHeaders = [String: String]
public typealias CompletionHandlerForTask = (_ data: Data?, _ error: Error?) -> Void


class Client: NSObject {
    
    
    func authenticate(with user: String, pass: String, completion: @escaping (_ success: Bool, _ errorString: NSError?) -> Void) {
        let jsonBody: [String : Any] = [BodyKeys.Username : user,
                                        BodyKeys.Password : pass]
        taskForPOST(method: Methods.Authentication, parameters: nil, jsonBody: jsonBody) { (data, error) in
            guard let data = data else {
                if let error = error {
                    print(error)
                }
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any],
                    let jsonObjectData = json["data"] as? [String : Any] {
                    let token = jsonObjectData["token"] as! String
                    Constants.userToken = token
                    completion(true,nil)
                }
            } catch {
                let userInfo = [NSLocalizedDescriptionKey : "Could not parse the data as JSON: '\(data)'"]
                completion(false, NSError(domain: "authenticate method", code: 1, userInfo: userInfo))
            }
        }
        
    }
    
    func taskForGET(_ method: String, parameters: ParemetersDictionary?, completionHandlerForGET: @escaping CompletionHandlerForTask) {
        
        let url = urlFromParameters(nil, withPathExtension: Methods.AllGames)
        
        var httpRequest = URLRequest(url: url)
        httpRequest.addValue(HeaderValues.ContentTypeValue, forHTTPHeaderField: HeaderKeys.ContentType)
        httpRequest.addValue("yRQYnWzskCZUxPwaQupWkiUzKELZ49eM7oWxAQK_ZXw1a1", forHTTPHeaderField: HeaderKeys.AuthorizationToken)
        
        let task = URLSession.shared.dataTask(with: httpRequest) { (data, response, error) in
            func sendError(_ error: String) {
                print(error)
                let userInfo = [NSLocalizedDescriptionKey : error]
                completionHandlerForGET(nil, NSError(domain: "taskForPOSTMethod", code: 1, userInfo: userInfo))
            }
            guard (error == nil) else {
                sendError("There was an error with your request: \(error!)")
                return
            }
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                sendError("Your request returned a status code other than 2xx!")
                return
            }
            guard let data = data else {
                sendError("No data was returned by the request!")
                return
            }
            completionHandlerForGET(data, nil)
        }
        task.resume()
    }
    
    

    func taskForPOST( method: String, parameters: ParemetersDictionary?, jsonBody: JSONDictionary?, completionHandlerForPOST: @escaping CompletionHandlerForTask) {
    
        let url = urlFromParameters(nil, withPathExtension: Methods.Authentication)
        
        var httpRequest = URLRequest(url: url)
        httpRequest.httpMethod = "POST"
        httpRequest.addValue(HeaderValues.ContentTypeValue, forHTTPHeaderField: HeaderKeys.ContentType)
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: jsonBody as Any, options: .prettyPrinted) {
            httpRequest.httpBody = jsonData
        }
        
        let task = URLSession.shared.dataTask(with: httpRequest) { data, response, error in
            func sendError(_ error: String) {
                print(error)
                let userInfo = [NSLocalizedDescriptionKey : error]
                completionHandlerForPOST(nil, NSError(domain: "taskForPOSTMethod", code: 1, userInfo: userInfo))
            }
            guard (error == nil) else {
                sendError("There was an error with your request: \(error!)")
                return
            }
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                sendError("Your request returned a status code other than 2xx!")
                return
            }
            guard let data = data else {
                sendError("No data was returned by the request!")
                return
            }
            completionHandlerForPOST(data, nil)
        }
        task.resume()
    }
    
    
    // create a URL from parameters
    private func urlFromParameters(_ parameters: ParemetersDictionary?, withPathExtension: String?) -> URL {
        var components = URLComponents()
        components.scheme = Constants.APIScheme
        components.host = Constants.APIHostMock
        components.path = withPathExtension ?? ""
        
        if parameters != nil {
            components.queryItems = [URLQueryItem]()
            for (key, value) in parameters! {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                components.queryItems!.append(queryItem)
            }
        }
        return components.url!
    }
}
