//
//  Constants.swift
//  IBM FC
//
//  Created by Silvia Florido on 12/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation


extension Client {
    
    struct Constants {
        static let APIScheme = "https"
        static let APIHostMock = "private-9b01bc-ibmfc.apiary-mock.com"
        static let APIHostProduction = "polls.apiblueprint.org"
        static var userToken: String?
    }
    
    struct Methods {
        static let Authentication = "/login"
        static let AllGames = "/games"
        static let AllStrikers = "/strikers"
    }
    
   
    struct HeaderKeys {
        static let AuthorizationToken = "Authorization"
        static let ContentType = "Content-Type"
    }
    struct HeaderValues {
        static let ContentTypeValue = "application/json"
    }
    
    // Authorization
    struct BodyKeys {
        static let Username = "username"
        static let Password = "password"
    }
   
    
    struct JSONResponseKeys {
        
        // Authorization
        static let Token = "token"
        static let Success = "success"
        static let Message = "message"
        static let Data = "data"
        static let Email = "email"
        
        // Games
        static let Status = "success"
        static let GameId = "gameId"
        static let TeamHomeId = "teamHomeId"
        static let TeamAwayId = "teamAwayId"
        static let ScoreHomeId = "scoreHomeId"
        static let ScoreAwayId = "scoreAwayId"
        
        // Players
        static let PlayerId = "playerId"
        static let PlayerName = "playerName"
        static let PlayerGoals = "goals"
        static let PlayerTeamId  = "teamId"
        static let PlayerPhotoUrl  = "playerPhotoURL"
    }
    
 }






