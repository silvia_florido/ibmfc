//
//  Team.swift
//  IBM FC
//
//  Created by Silvia Florido on 09/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation


enum GameResult: Int {
    case win = 3
    case even = 1
    case defeat = 0
}


class Team: NSObject {
    
    let id: String
    let name: String
    var pathToFlag: String
    private (set) var wins: Int
    private (set) var defeats: Int
    private (set) var evens: Int
    var goals: Int
    
    var totalPoints: Int {
        let points = (wins * GameResult.win.rawValue) + (evens * GameResult.even.rawValue)
        
        return points
    }
    
    init(id: String, name: String, pathToFlag: String) {
        self.id = id
        self.name = name
        self.pathToFlag = pathToFlag
        wins = 0
        defeats = 0
        evens = 0
        goals = 0
    }
    
    func setGameResults(result: GameResult, goals: Int) {
        self.goals += goals
        
        switch result {
        case .win:
            wins += 1
        case .even:
            evens += 1
        case .defeat:
            defeats += 1
        }
    }
    
    
}





