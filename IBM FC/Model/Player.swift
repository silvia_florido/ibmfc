//
//  Player.swift
//  IBM FC
//
//  Created by Silvia Florido on 09/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation

class Player: NSObject {
    
    let id: Int
    let name: String
    let goals: Int
    let teamId: Int
    let photoURL: String
    
    
    init(id: Int, name: String, goals: Int, teamId: Int, photoURL: String) {
        self.id = id
        self.name = name
        self.goals = goals
        self.teamId = teamId
        self.photoURL = photoURL
    }
  
}
