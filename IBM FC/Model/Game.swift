//
//  Game.swift
//  IBM FC
//
//  Created by Silvia Florido on 09/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation


class Game: NSObject {
    
    static var nextUid: Int = 0
    static func generateUid() -> Int {
        nextUid += nextUid
        return nextUid
    }
    
    let homeTeam: Team
    let awayTeam: Team
    var homeTeamScore: Int
    var awayTeamScore: Int
    var isEven: Bool = true
    
    
    init(homeTeam: Team, awayTeam: Team, homeTeamScore: Int, awayTeamScore: Int) {
        self.homeTeam = homeTeam
        self.awayTeam = awayTeam
        self.homeTeamScore = homeTeamScore
        self.awayTeamScore = awayTeamScore
    }
    
    init(homeTeam: Team, awayTeam: Team) {
        self.homeTeam = homeTeam
        self.awayTeam = awayTeam
        homeTeamScore = 0
        awayTeamScore = 0
    }
    
   
    func calculateGameResults() {
        switch homeTeamScore {
        case _ where homeTeamScore > awayTeamScore:
            homeTeam.setGameResults(result: .win, goals: homeTeamScore)
            awayTeam.setGameResults(result: .defeat, goals: awayTeamScore)
            isEven = false
        case _ where homeTeamScore < awayTeamScore:
            homeTeam.setGameResults(result: .defeat, goals: homeTeamScore)
            awayTeam.setGameResults(result: .win, goals: awayTeamScore)
            isEven = false
        case _ where homeTeamScore == awayTeamScore:
            homeTeam.setGameResults(result: .even, goals: homeTeamScore)
            awayTeam.setGameResults(result: .even, goals: awayTeamScore)
            isEven = true
        default:
            break
        }
    }
    
    func setScore(score: Int, for team: Team) {
        if team === homeTeam || team === awayTeam {
            team.goals = score
        }
    }
    

    
}
